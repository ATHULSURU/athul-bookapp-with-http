import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../services/book.service';
import { Book } from '../Modules/Book';


@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.css']
})
export class ListBookComponent implements OnInit {

  public book: Book[] = [];

  constructor(private bookservice: BookService, private route: Router) { }

  ngOnInit() {
    this.bookservice.getBooks().subscribe(data => { this.book = data; });
  }
  deleteBook(i, id): void {
    this.bookservice.deleteBook(id).subscribe(() => {
      this.book.splice(i, 1);
    });

  }

  onSelect(id) {
    this.route.navigate(['/book-details', id]);
  }
  toUpdate(i) {
    this.route.navigate(['/update-book', i]);
  }

}
