package io.book.books;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

@Entity
public class Books {
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;	
	@NotNull
	@Length(min=5, max=5)
	private String slno;
	@NotNull
	private String bookName;
	@NotNull
	private String authorName;
	@NotNull
	@Length(min=5, max=500)
	private String description;


	public Books() {

	}


	public Books(String slno, String bookName, String authorName, String description) {
		super();
		this.slno = slno;
		this.bookName = bookName;
		this.authorName = authorName;
		this.description = description;
	}

	public String getSlno() {
		return slno;
	}
	public void setSlno(String slno) {
		this.slno = slno;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


}
