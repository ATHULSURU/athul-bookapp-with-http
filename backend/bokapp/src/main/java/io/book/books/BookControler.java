package io.book.books;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin( origins ="http://localhost:4200")
public class BookControler {
	
	
	
	@Autowired
	private BookService bookservice;
	
	
	@RequestMapping("/books")	
	public List<Books> getAllTopic() {
		return bookservice.getAllBooks();
	}
	@RequestMapping("/books/{id}")
	public Optional<Books> getTopics(@PathVariable Integer id){
		return bookservice.getBook(id);
	}
	@PostMapping(value="/books")
    public void addBook(@RequestBody Books book)
    {
		bookservice.addBook(book);
    }
	
	@PutMapping(value="/books/{id}")
	public void updateBook(@RequestBody Books book,@PathVariable Integer id)
	{
		bookservice.updateBook(book,id);
	}
	@DeleteMapping(value="/books/{id}")
	public void deleteBook(@PathVariable Integer id){
		bookservice.deleteBook(id);
	}

}
