import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found-component',
  template: `<h3 class="text-danger "><i class="material-icons">error</i> Page Not Found...</h3>`
})
export class NotFoundComponentComponent {

  constructor() { }

}
