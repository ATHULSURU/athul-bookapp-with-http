import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { throwError as observableThrowError, Observable } from 'rxjs';
import { Book } from '../Modules/Book';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private httpOptions = { headers: this.headers };
  private url = 'http://localhost:8080/books/';

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.url);
  }
  createBook(data): Observable<Book> {
    return this.http.post<Book>(this.url, JSON.stringify(data), this.httpOptions);
  }
  deleteBook(id): Observable<Book> {
    return this.http.delete<Book>(this.url + id, this.httpOptions);
  }
  getBook(index): Observable<Book> {
    return this.http.get<Book>(this.url + index);
  }
  savebook(data, index) {
    return this.http.put<Book>(this.url + index, JSON.stringify(data), this.httpOptions);
  }

}
