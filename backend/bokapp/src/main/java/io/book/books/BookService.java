package io.book.books;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
	
	@Autowired
	private BookRepocitry bookrepocitry;
	
	
	public List<Books> getAllBooks(){
		
		List<Books> books=new ArrayList<>();
		bookrepocitry.findAll().forEach(books::add);
		return books;
	}
	
	public Optional<Books> getBook(Integer id) {
		return bookrepocitry.findById(id);
	}
	
	public Books addBook(Books book) {
		return bookrepocitry.save(book);
	}
	
	public Books updateBook(Books book, Integer id) {
		
		book.setId(id);
		return bookrepocitry.save(book);
		
	} 
	
	public void deleteBook(Integer id) {
		
		bookrepocitry.deleteById(id);
	}

}
