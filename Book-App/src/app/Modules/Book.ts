export interface Book {
        id: number;
        Slno: string;
        bookName: string;
        authorName: string;
        description: string;
}
